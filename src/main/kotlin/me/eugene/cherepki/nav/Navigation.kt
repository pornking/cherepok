package me.eugene.cherepki.nav

import androidx.compose.runtime.*
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import java.util.*

interface Navigation {
    val currentScreen: StateFlow<Screen>
    val canBack: MutableState<Boolean>

    fun navigateTo(screen: Screen)
    fun back(): Boolean
}

class NavigationImpl : Navigation {
    private val stack = LinkedList<Screen>()
    override val canBack: MutableState<Boolean> = mutableStateOf(false)

    init {
        stack.add(startScreen)
    }

    override val currentScreen = MutableStateFlow<Screen>(startScreen)

    override fun navigateTo(screen: Screen) {
        stack.add(screen)
        currentScreen.value = screen
        updateRoot()
    }

    private fun updateRoot() {
        canBack.value = stack.size > 1
    }

    override fun back(): Boolean {
        if (stack.isEmpty()) {
            return false
        }
        stack.removeLast()
        currentScreen.value = stack.last
        updateRoot()
        return true
    }

    companion object {
        private val startScreen = Screen.SelectFolderScreen()
    }
}

private val navigationComposition = NavigationImpl()

fun getNav(): Navigation = navigationComposition

@Composable
fun getCurScreen(): State<Screen> = getNav().currentScreen.collectAsState()
