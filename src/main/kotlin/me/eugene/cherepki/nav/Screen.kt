package me.eugene.cherepki.nav

import me.eugene.cherepki.core.FastbootCommand
import java.io.File

sealed class Screen(val title: String) {
    class SelectFolderScreen : Screen("Выберите папку с образом")
    class FlashScreen(val commandList: List<FastbootCommand>) : Screen("Делаем черепок")
}
