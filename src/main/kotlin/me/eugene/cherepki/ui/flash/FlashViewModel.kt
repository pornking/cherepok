package me.eugene.cherepki.ui.flash

import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import me.eugene.cherepki.core.CommandExecutor
import me.eugene.cherepki.core.CommandExecutorImpl
import me.eugene.cherepki.core.FastbootCommand
import me.eugene.cherepki.ui.main.ViewModel

class FlashViewModel(
    initCommandList: List<FastbootCommand>,
    private val commandExecutor: CommandExecutor = CommandExecutorImpl()
) : ViewModel() {
    private val _isFlashing = MutableStateFlow(false)
    val isFlashing: StateFlow<Boolean> get() = _isFlashing
    private val _list = MutableStateFlow(initCommandList)
    val list: StateFlow<List<FastbootCommand>> get() = _list

    fun flash() {
        launch {
            var commands = _list.value.toMutableList()
            if (commands.isEmpty()) {
                return@launch
            }
            _isFlashing.value = true
            while (commands.any { !it.installed }) {
                val command = commands.first { !it.installed }
                try {
                    commandExecutor.execute(command.command) {
                        println("progress updated: $it")
                    }
                    commands[commands.indexOf(command)] = command.copy(installed = true)
                    _list.value = commands.map { it.copy() }
                } catch (e: Exception) {
                    e.printStackTrace()
                    _isFlashing.value = false
                    return@launch
                }
            }
            println("FLASHING COMPLETED")
            _isFlashing.value = false
        }
    }

    fun cancelFlash() {
        launch {
            commandExecutor.cancel()
        }
    }
}
