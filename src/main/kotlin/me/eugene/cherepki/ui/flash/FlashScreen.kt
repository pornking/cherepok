package me.eugene.cherepki.ui.flash

import androidx.compose.desktop.ui.tooling.preview.Preview
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import me.eugene.cherepki.core.FastbootCommand
import me.eugene.cherepki.nav.getNav

@Composable
@Preview
fun FlashScreen(flashViewModel: FlashViewModel) {
    val isFlashing = flashViewModel.isFlashing.collectAsState()
    val items = flashViewModel.list.collectAsState()
    val currentItem = if (isFlashing.value) {
        items.value.first { !it.installed }
    } else {
        null
    }
    getNav().canBack.value = !isFlashing.value
    Column {
        LazyColumn {
            items(items = items.value, key = { it.name }) { DrawCommand(it, currentItem) }
        }
        Button(
            modifier = Modifier.fillMaxWidth().padding(4.dp),
            onClick = {
            if (isFlashing.value) {
                flashViewModel.cancelFlash()
            } else {
                flashViewModel.flash()
            }
        }) {
            val text = if (isFlashing.value) "Отменить" else "Сделать черепок"
            Text(text)
        }
    }
}

@OptIn(ExperimentalMaterialApi::class)
@Composable
@Preview
fun DrawCommand(command: FastbootCommand, curCommand: FastbootCommand?) {
    Column(modifier = Modifier.fillMaxWidth().clickable() {}) {
        ListItem(
            text = { Text(command.name) },
            secondaryText = {
                if (command.file != null) {
                    val text = if (command.installed) "Обновлено" else "Не обновлено"
                    Text(text)
                }
            },
        )
        if (command == curCommand) {
            LinearProgressIndicator(modifier = Modifier.fillMaxWidth())
        }
        Divider(modifier = Modifier.height(0.5.dp))
    }
}


