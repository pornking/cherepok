package me.eugene.cherepki.ui.main

import androidx.compose.desktop.DesktopMaterialTheme
import androidx.compose.desktop.ui.tooling.preview.Preview
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.window.Window
import androidx.compose.ui.window.application
import me.eugene.cherepki.nav.Screen
import me.eugene.cherepki.nav.getCurScreen
import me.eugene.cherepki.nav.getNav
import me.eugene.cherepki.ui.flash.FlashScreen
import me.eugene.cherepki.ui.flash.FlashViewModel
import java.awt.FileDialog
import java.awt.Frame
import java.io.File


@Composable
@Preview
fun App() {
    DesktopMaterialTheme {
        Scaffold(
            topBar = { AppToolbar() }
        ) {
            Route()
        }
    }
}

@Composable
@Preview
fun Route() {
    val navState = getNav().currentScreen.collectAsState()
    when (val state = navState.value) {
        is Screen.FlashScreen -> { FlashScreen(FlashViewModel(state.commandList)) }
        is Screen.SelectFolderScreen -> SelectFolderScreen()
    }
}

@Composable
@Preview
fun SelectFolderScreen() {
    val fileState = remember { mutableStateOf<File?>(null) }
    Column(
        modifier = Modifier.fillMaxSize(1f),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center,
    ) {
        Button(
            onClick = {
                FileDialog {
                    if (selectFolderViewModel.checkFolder(it)) {
                        fileState.value = it
                    }
                }
            }) {
            Text("Выберите файл")
        }
        if (fileState.value != null) {
            val commands = selectFolderViewModel.buildCommands(fileState.value!!)
            getNav().navigateTo(Screen.FlashScreen(commands))
        }
    }
}

private fun FileDialog(
    parent: Frame? = null,
    onCloseRequest: (result: File?) -> Unit
) {
    object : FileDialog(parent, "Choose a file", LOAD) {
        override fun setVisible(value: Boolean) {
            super.setVisible(value)
            if (value) {
                val file: File? = if (directory == null) null else File(directory!!)
                onCloseRequest(file)
            }
        }
    }.isVisible = true
}

@Composable
@Preview
fun AppToolbar() {
    TopAppBar(
        title = { Text(getCurScreen().value.title) },
        navigationIcon = {
            if (getNav().canBack.value) {
                IconButton(
                    onClick = { getNav().back() }
                ) {
                    Icon(Icons.Default.ArrowBack, "Вернуться назад")
                }
            }
        }
    )
}

fun main() = application {
    Window(onCloseRequest = ::exitApplication, title = "Черепок Inc.") {
        App()
    }
}
