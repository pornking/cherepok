package me.eugene.cherepki.ui.main

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import me.eugene.cherepki.core.FastbootCommand
import java.io.File

abstract class ViewModel : CoroutineScope {
    override val coroutineContext = Dispatchers.Main
}

class SelectFolderViewModel : ViewModel() {

    fun checkFolder(folder: File?): Boolean {
        folder ?: return false
        return folder.exists() && buildCommands(folder).isNotEmpty()
    }

    fun buildCommands(folder: File): List<FastbootCommand> {
        val commands = mutableListOf<FastbootCommand>()
        val boot = File(folder, "boot.img")
        val system = File(folder, "system.img")
        val recovery = File(folder, "recovery.img")
        val cache = File(folder, "cache.img")
        val userdata = File(folder, "userdata.img")
        if (boot.exists()) {
            commands.add(FastbootCommand(boot, "boot"))
        }
        if (system.exists()) {
            commands.add(FastbootCommand(system, "system"))
        }
        if (recovery.exists()) {
            commands.add(FastbootCommand(recovery, "recovery"))
        }
        if (cache.exists()) {
            commands.add(FastbootCommand(cache, "cache"))
        }
        if (userdata.exists()) {
            commands.add(FastbootCommand(userdata, "userdata"))
        }
        if (commands.isNotEmpty()) {
            commands.add(FastbootCommand(null, "Перезагрузка", false, "fastboot reboot"))
        }
        return commands
    }
}

val selectFolderViewModel = SelectFolderViewModel()
