package me.eugene.cherepki.core

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.withContext


interface CommandExecutor {
    suspend fun execute(command: String, callback: (String) -> Unit)
    suspend fun cancel()
}

class FakeCommandExecutor : CommandExecutor {
    private var isCancelled = false

    override suspend fun execute(command: String, callback: (String) -> Unit) {
        isCancelled = false
        repeat(3) {
            delay(1000)
            if (isCancelled) {
                throw IllegalStateException("cancelled")
            } else {
                callback.invoke("msg $it")
            }
        }
    }

    override suspend fun cancel() {
        isCancelled = true
    }

}

class CommandExecutorImpl : CommandExecutor {
    private var process: Process? = null

    override suspend fun execute(command: String, callback: (String) -> Unit) {
        withContext(Dispatchers.IO) {
            val p = ProcessBuilder(command.split(" "))
                .inheritIO()
                .start()
            try {
                process = p
                p.waitFor()
                val result = process?.exitValue() ?: throw IllegalStateException("cancelled")
                if (result != 0) {
                    throw IllegalStateException("Failed with code $result")
                }
            } finally {
                p.destroy()
            }
        }
    }

    override suspend fun cancel() {
        withContext(Dispatchers.IO) {
            process?.destroyForcibly()?.waitFor()
            process = null
        }
    }
}
