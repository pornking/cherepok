package me.eugene.cherepki.core

import java.io.File

data class FastbootCommand(
    val file: File?,
    val name: String,
    val installed: Boolean = false,
    val command: String = "fastboot flash $name ${file!!.absolutePath}"
) {

    init {
        file?.let {
            if (!it.exists()) {
                throw IllegalStateException("file ${file.absolutePath} must be exists")
            }
        }
    }
}
